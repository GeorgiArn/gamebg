<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GameController@index')->name('home');

Auth::routes();

Route::resource('game', 'Admin\GameController', ['only' => ['create', 'store', 'edit', 'update', 'destroy']]);
Route::resource('game', 'GameController', ['only' => ['show', 'index']]);

Route::resource('user', 'UserController', ['only' => ['show', 'edit', 'update']]);
Route::resource('user', 'Admin\UserController', ['only' => ['destroy']]);

Route::resource('category', 'CategoryController', ['only' => ['store', 'update', 'destroy']]);

Route::post('/user/games/{game}/add', 'UserGameController@store')->name('user.games.add');
Route::delete('/user/games/{game}/remove', 'UserGameController@destroy')->name('user.games.remove');

Route::post('/user/friends/{user}/add', 'UserFriendController@store')->name('user.friends.add');
Route::delete('/user/friends/{user}/remove', 'UserFriendController@destroy')->name('user.friends.remove');

Route::post('/user/{user}/messages/add', 'UserMessageController@store')->name('user.messages.add');
Route::get('/user/{user}/messages', 'UserMessageController@index')->name('user.messages.index');

Route::get('/password/{user}/edit', 'ChangePasswordController@edit')->name('user.password.edit');
Route::put('/password/{user}/update', 'ChangePasswordController@update')->name('user.password.update');

Route::get('/search-user', 'SearchUserController@index')->name('user.search.index');

Route::get('/admin', 'AdminController@show')->name('admin.show');
