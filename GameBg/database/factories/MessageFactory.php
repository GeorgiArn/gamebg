<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Message::class, function (Faker $faker) {
    return [
        'content' => $faker->text,
        'sender_id' => factory(\App\User::class),
        'receiver_id' => factory(\App\User::class)
    ];
});
