<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\GameImage::class, function (Faker $faker) {
    // Image
    $sourceDir = public_path('test/images/');
    $targetDir = public_path('games');
    $imageName = $faker->file($sourceDir, $targetDir, false);

    return [
        'game_id' => factory(\App\Game::class),
        'image' => 'games/'.$imageName,
        'is_main_image' => 0
    ];
});
