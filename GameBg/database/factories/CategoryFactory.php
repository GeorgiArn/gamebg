<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Category::class, function (Faker $faker) {
    $name = $faker->unique()->name;
    $slug = \Illuminate\Support\Str::slug($name, '-');

    // Image
    $sourceDir = public_path('test/images/');
    $targetDir = public_path('categories');
    $imageName = $faker->file($sourceDir, $targetDir, false);

    return [
        'slug' => $slug,
        'name' => $name,
        'image' => 'categories/'.$imageName
    ];
});
