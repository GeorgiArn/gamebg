<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Game::class, function (Faker $faker) {
    $name = $faker->unique()->name;
    $slug = \Illuminate\Support\Str::slug($name, '-');

    return [
        'slug' => $slug,
        'name' => $name,
        'description' => $faker->text,
        'category_id' => factory(\App\Category::class)
    ];
});
