<?php

use Illuminate\Database\Seeder;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Game::class, 10)->create()->each(function ($game) {
            factory(\App\GameImage::class)->create(['is_main_image'=>true, 'game_id' => $game->id]);
            factory(\App\GameImage::class, 5)->create(['game_id' => $game->id]);
        });
    }
}
