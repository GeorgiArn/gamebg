<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 10)->create()->each(function ($user) {
            $user->assignRole(\App\Role::find(rand(1,2)));
            $user->addGame(\App\Game::all()->random());
        });

        foreach (\App\User::all() as $currUser) {
            $friend = App\User::where('id', '!=', $currUser->id)->whereNotIn('id', $currUser->friends())->inRandomOrder()->first();
            $currUser->addFriend($friend);
            factory(\App\Message::class)->create(['sender_id' => $currUser->id, 'receiver_id' => $friend->id]);
        }
    }
}
