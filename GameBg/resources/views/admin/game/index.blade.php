<div class="card m2 mt-5">
    <div class="card-header">Games</div>
    <div class="card-body ">
        <a class="btn btn-info" href="{{ route('game.create') }}">Add game</a>
        <table id="games-table" text-center class="display" style="width:100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Category</th>
                <th>Added</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($games as $game)
                <tr>
                    <td>{{$game->name}}</td>
                    <td>{{$game->category->name}}</td>
                    <td>{{$game->created_at->diffForHumans()}}</td>
                    <td><a class="btn btn-info" href="{{ route('game.show', $game->slug) }}">View</a></td>
                    <td>
                        <form method="POST" action="{{route('game.destroy', $game->slug)}}">
                            @csrf
                            @method("DELETE")
                            <button type=submit class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
