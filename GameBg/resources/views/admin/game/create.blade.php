@extends('layouts.app')

@section('scripts')
    <script src="{{ url('/js/game/upload-game-images.js') }}" type="text/javascript"></script>
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ url('/css/game/game-images-placeholders.css') }}" />
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card m2">
                    <div class="card-header">Add game</div>
                    <div class="card-body text-center">
                        <form method="POST" action="{{route('game.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">Name</label>

                                <div class="col-md-8">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" required  autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-md-2 col-form-label text-md-right">Description</label>
                                <div class="col-md-8">
                                    <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description" required>
                                    </textarea>

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category" class="col-md-2 col-form-label text-md-right">Category</label>

                                <div class="col-md-8">
                                    <select name="category" class="form-control" >
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}"> {{$category->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('category')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="form-group row">

                                <label for="images" class="col-md-2 col-form-label text-md-right">Images</label>

                                <div class="col-md-8">

                                    <div class="wrap-custom-file">
                                        <input type="file" name="images[]" id="game-image-1" />
                                        <label for="game-image-1">
                                            <span>Main Game Image</span>
                                            <i class="fa fa-plus-circle"></i>
                                        </label>
                                    </div>
                                    <div class="wrap-custom-file">
                                        <input type="file" name="images[]" id="game-image-2"/>
                                        <label for="game-image-2">
                                            <span>Game Image</span>
                                            <i class="fa fa-plus-circle"></i>
                                        </label>
                                    </div>
                                    <div class="wrap-custom-file">
                                        <input type="file" name="images[]" id="game-image-3"/>
                                        <label for="game-image-3">
                                            <span>Game Image</span>
                                            <i class="fa fa-plus-circle"></i>
                                        </label>
                                    </div>
                                    <div class="wrap-custom-file">
                                        <input type="file" name="images[]" id="game-image-4"/>
                                        <label for="game-image-4">
                                            <span>Game Image</span>
                                            <i class="fa fa-plus-circle"></i>
                                        </label>
                                    </div>
                                    <div class="wrap-custom-file">
                                        <input type="file" name="images[]" id="game-image-5"/>
                                        <label for="game-image-5">
                                            <span>Game Image</span>
                                            <i class="fa fa-plus-circle"></i>
                                        </label>
                                    </div>
                                    <div class="wrap-custom-file">
                                        <input type="file" name="images[]" id="game-image-6"/>
                                        <label for="game-image-6">
                                            <span>Game Image</span>
                                            <i class="fa fa-plus-circle"></i>
                                        </label>
                                    </div>
                                </div>
                                @error('images')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                @error('images.*')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6">
                                    <a class="btn btn-secondary" href="{{url()->previous()}}">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
