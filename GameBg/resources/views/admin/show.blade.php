@extends('layouts.app')

@section('scripts')
    <script src="{{ url('/js/admin/admin-datatables.js') }}" type="text/javascript"></script>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('admin.category.index')
            <div class="col-md-10">
                @include('admin.user.index')
                @include('admin.game.index')
            </div>
        </div>
    </div>
    @include('admin.category.create')
@endsection
