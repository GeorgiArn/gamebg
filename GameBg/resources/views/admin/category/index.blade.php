<div class="col-md-2">
    <div class="card-header">Categories <a class="btn btn-sm btn-info text-center"  data-toggle="modal" data-target="#createCategoryModal">Create category</a></div>
    <div class="card-body">
        @foreach($categories as $category)
            <p><img class="rounded-circle" src="{{$category->getImage()}}" width="30" height="30"><b> {{$category->name}}</b></p>
            <a class="btn btn-sm btn-info text-center" data-toggle="modal" data-target="#editCategoryModal{{$category->slug}}">Edit</a>
            <form method="POST" action="{{route('category.destroy', $category->slug)}}">
                @csrf
                @method("DELETE")
                <button type="submit" class="btn btn-sm btn-danger text-center">Delete</button>
            </form>
            <hr>
            @include('admin.category.edit')
        @endforeach
    </div>
</div>
