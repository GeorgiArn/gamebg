<div class="card m2">
    <div class="card-header">Users</div>
    <div class="card-body text-center">
        <table id="users-table" class="display" style="width:100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Joined</th>
                <th>Games (count)</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->username}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->created_at->diffForHumans()}}</td>
                    <td>{{$user->games->count()}}</td>
                    <td><a class="btn btn-info" href="{{route('user.show', $user->username)}}">View</a></td>
                    <td>
                        <form method="POST" action="{{route('user.destroy', $user->username)}}">
                            @csrf
                            @method("DELETE")
                            <button type=submit class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
