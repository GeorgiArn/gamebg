@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{$game->name}}</div>
                    <div class="card-body text-center">
                        <img class="rounded mb-2" src="{{ $game->mainImage()->getImage() }}" width="100" height="100">
                        <h2> <b>{{$game->name}}</b> </h2>

                        <p>{{$game->description}}</p>
                        <h4><b>Game Images:</b></h4>
                        @foreach($game->images as $image)
                            @if(!$image->is_main_image)
                                <img class="rounded" src="{{ $image->getImage() }}" width="100" height="100">
                            @endif
                        @endforeach
                        @auth()
                            @if(!$game->isPlayedBy(auth()->user()))
                                <form method="post" action="{{ route('user.games.add', $game->slug) }}">
                                    @csrf
                                    <button class="mt-3" type="submit" >I play this</button>
                                </form>
                            @else
                                <form method="post" action="{{ route('user.games.remove', $game->slug) }}">
                                    @method("delete")
                                    @csrf
                                    <button class="mt-3" type="submit">I don't play this anymore</button>
                                </form>
                            @endif
                                <form method="GET" action="{{route('user.search.index')}}">
                                    @csrf
                                    <input type="hidden" name="games[]" value="{{$game->slug}}">
                                    <input class="mt-2" type="submit" value="People who play this game">
                                </form>
                            @if(auth()->user()->isAdmin())
                                <a class="btn btn-info mt-2" href="{{route('game.edit', $game->slug)}}">Edit Game</a>
                            @endif
                        @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
