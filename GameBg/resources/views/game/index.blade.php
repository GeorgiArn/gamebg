@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @auth
            @include('user.user-game.index')
        @endauth
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">All Games</div>

                <div class="card-body text-center">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @foreach($games as $game)
                            <p>{{$game->name}}</p>
                            <p> Category: <img class="rounded-circle" src="{{ $game->category->getImage() }}" width="25" height="25"> {{$game->category->name}}</p>
                            <a class="btn btn-info btn-sm mb-2" href="{{route('game.show', $game->slug)}}">View game</a>
                            @auth()
                                @if(!$game->isPlayedBy(auth()->user()))
                                    <form method="post" action="{{route('user.games.add', $game->slug)}}">
                                        @csrf
                                        <button type="submit">I play this</button>
                                    </form>
                                @else
                                    <form method="post" action="{{route('user.games.remove', $game->slug)}}">
                                        @method("delete")
                                        @csrf
                                        <button type="submit">I don't play this anymore</button>
                                    </form>
                                @endif
                            @endauth
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
            @auth()
                <div class="col-md-2">
                    <div class="card">
                        <div class="card-header">Search buddy</div>
                        <div class="card-body text-center">
                            <form method="GET" action="{{route('user.search.index')}}">
                                @csrf
                                <label for="games">Choose a game:</label>
                                <select name="games[]" multiple="multiple">
                                    @foreach($games as $game)
                                        <option value="{{$game->slug}}">{{$game->name}}</option>
                                    @endforeach
                                </select>
                                <br><br>
                                <input type="submit" value="Submit">
                            </form>
                        </div>
                    </div>
                </div>
            @endauth

    </div>
</div>
@endsection
