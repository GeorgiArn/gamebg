@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @include('user.user-game.index')
            <div class="col-md-8">
                <div class="card m2">
                    <div class="card-header">{{$user->username}}'s profile</div>
                    <div class="card-body text-center">
                        @if($user->image !== null)
                            <img class="rounded-circle" src="{{ $user->getImage() }}" width="125" height="125">
                        @endif
                        <h2 class="mt-3">{{$user->name}}</h2>
                        @can('update-user', $user)
                            <a class="btn btn-secondary" href="{{route('user.edit', $user->username)}}">Edit profile</a>
                                <a class="btn btn-secondary" href="{{route('user.password.edit', $user->username)}}">Change password</a>
                        @else
                            @if(auth()->user()->isFriend($user))
                                    <form method="post" action="{{route('user.friends.remove', $user->username)}}">
                                        @csrf
                                        @method("DELETE")
                                        <button type="submit">Remove from friends</button>
                                    </form>
                                    <form method="post" action="{{route('user.messages.add', $user->username)}}">
                                        @csrf
                                        <input type="text" name="content">
                                        <button type="submit">Send Message</button>
                                    </form>
                            @else
                                    <form method="post" action="{{route('user.friends.add', $user->username)}}">
                                        @csrf
                                        <button type="submit">Add to friends</button>
                                    </form>
                            @endif
                        @endcan
                    </div>
                </div>
            </div>
            @include('user.user-friend.index')
        </div>
    </div>
@endsection
