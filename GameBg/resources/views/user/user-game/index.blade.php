<div class="col-md-2">
    <div class="card m2">
        @if(auth()->user()->id === $user->id)
            <div class="card-header">I play this:</div>
        @else
            <div class="card-header">{{$user->username}} plays this:</div>
        @endif

        <div class="card-body text-center">
            @foreach($user->games as $game)
                <p>{{$game->name}}</p>
                <p> Category: {{$game->category->name}}</p>
                @if(auth()->user()->id === $user->id)
                    @if(!$game->isPlayedBy(auth()->user()))
                        <form method="post" action="{{route('user.games.add', $game->slug)}}">
                            @csrf
                            <button type="submit">I play this</button>
                        </form>
                    @else
                        <form method="post" action="{{route('user.games.remove', $game->slug)}}">
                            @method("delete")
                            @csrf
                            <button type="submit">I don't play this anymore</button>
                        </form>
                    @endif
                @endif
                <hr>
            @endforeach
        </div>
    </div>
</div>
