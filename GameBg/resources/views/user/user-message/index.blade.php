@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
                <div class="col-md-2">
                    <div class="card">
                        <div class="card-header">Chat with your friends:</div>
                        <div class="card-body text-center">
                            @foreach(auth()->user()->friends() as $friend)
                                <p>{{$friend->name}}</p>
                                <a class="btn btn-info" href="{{route('user.messages.index', $friend->username)}}">Chat</a>
                                <hr>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="card m2">
                        <div class="card-header">Chat with {{$user->name}}</div>
                        <div class="card-body">
                            @foreach(auth()->user()->messagesTimelineByUser($user) as $message)

                                @if(auth()->user()->isReceiver($message))
                                    <div>
                                        <p><b>{{$user->username}}:</b> {{$message->content}}</p>
                                    </div>
                                    <br>
                                @else
                                    <div>
                                        <p><b>Me:</b> {{$message->content}}</p>
                                    </div>
                                    <br>
                                @endif
                            @endforeach
                        </div>
                        <form method="post" action="{{route('user.messages.add', $user->username)}}">
                            @csrf
                            <textarea name="content"></textarea>
                            @error('content')
                            <p>{{$message}}</p>
                            @enderror
                            <button type="submit">Send Message</button>
                        </form>
                    </div>
                </div>
        </div>
    </div>
@endsection
