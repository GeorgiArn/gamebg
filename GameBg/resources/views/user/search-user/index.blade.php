@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="card m2">
                    <div class="card-header">Buddies</div>
                    <div class="card-body text-center">
                        @foreach($players as $id => $player)
                            @if($player->id !== auth()->user()->id)
                                <p>{{$player->name}}</p>
                                <p><b>Found by this games:</b></p>
                                @foreach($player->games as $playerGame)
                                    @if(in_array($playerGame->slug, $games))
                                        <p>{{ $playerGame->name}}</p>
                                    @endif
                                @endforeach
                            <a href="{{route('user.show', $player->username)}}" class="btn btn-info">View profile</a>
                                <hr>
                            @endif


                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
