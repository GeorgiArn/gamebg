<div class="col-md-2">
    <div class="card">
        @if(auth()->user()->id === $user->id)
            <div class="card-header">My friends:</div>
        @else
            <div class="card-header">{{$user->username}}'s friends:</div>
        @endif
        <div class="card-body text-center">
            @foreach($user->friends() as $friend)
                <p>{{$friend->name}} ({{$friend->username}})</p>
                <a class="btn btn-info" href="{{route('user.show', $friend->username)}}">View profile</a>
                @if(auth()->user()->isFriend($friend))
                    <a class="btn btn-info mt-2" href="{{route('user.messages.index', $friend->username)}}">Send message</a>
                @endif
                <hr>
            @endforeach

        </div>
    </div>
</div>
