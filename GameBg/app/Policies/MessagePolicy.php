<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;

    public function store(User $currUser, User $receiver)
    {
        return auth()->user()->isFriend($receiver);
    }
}
