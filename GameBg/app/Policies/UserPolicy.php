<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function update(User $user, User $currUser)
    {
        return $user->id === $currUser->id;
    }

    public function destroy(User $user)
    {
        return $user->isAdmin();
    }
}
