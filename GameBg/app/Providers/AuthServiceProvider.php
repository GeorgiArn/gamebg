<?php

namespace App\Providers;

use App\Policies\UserPolicy;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('store-category', 'App\Policies\CategoryPolicy@store');
        Gate::define('update-category', 'App\Policies\CategoryPolicy@update');

        Gate::define('update-user', 'App\Policies\UserPolicy@update');
        Gate::define('destroy-user', 'App\Policies\UserPolicy@destroy');

        Gate::define('store-game', 'App\Policies\GamePolicy@store');
        Gate::define('update-game', 'App\Policies\GamePolicy@update');
        Gate::define('destroy-game', 'App\Policies\GamePolicy@destroy');

        Gate::define('store-message', 'App\Policies\MessagePolicy@store');
    }
}
