<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMessage;
use App\Message;
use App\User;
use Illuminate\Http\Request;

class UserMessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(User $user)
    {
        $this->authorize('store-message', $user);

        return view('user/user-message/index', compact('user'));
    }

    public function store(User $user, StoreMessage $request)
    {
        $attributes = $request->validated();

        Message::create([
            'sender_id' => auth()->user()->id,
            'receiver_id' => $user->id,
            'content' => $attributes['content']
        ]);

        return redirect(route('user.messages.index', $user->username));
    }
}
