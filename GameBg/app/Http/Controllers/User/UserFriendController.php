<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserFriendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(User $user)
    {
        auth()->user()->addFriend($user);

        return back();
    }

    public function destroy(User $user)
    {
        auth()->user()->removeFriend($user);

        return back();
    }
}
