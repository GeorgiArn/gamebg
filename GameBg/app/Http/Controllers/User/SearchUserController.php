<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SearchUserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $games = $request->input('games');

        $players = User::whereHas('games', function ($query) use ($games){
            $query->whereIn('games.slug', $games);
        })->get();

        return view('user/search-user/index', compact('players', 'games'));
    }
}
