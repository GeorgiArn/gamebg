<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserBasicData;
use App\Http\Requests\UpdateUserPassword;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(User $user)
    {
        return view('user/show', compact('user'));
    }

    public function edit(User $user)
    {
        $this->authorize('update-user', $user);

        return view('user/edit', compact('user'));
    }

    public function update(User $user, UpdateUserBasicData $request)
    {
        $attributes = $request->validated();

        if ($request->file('image')) {
            $attributes['image'] = $request->file('image')->store('profileImages');
        }

        $user->update($attributes);

        return $this->show($user);
    }
}
