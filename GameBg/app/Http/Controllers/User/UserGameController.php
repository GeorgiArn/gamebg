<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserGameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Game $game)
    {
        auth()->user()->addGame($game);

        return back();
    }

    public function destroy(Game $game)
    {
        auth()->user()->removeGame($game);

        return back();
    }
}
