<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function edit(User $user)
    {
        $this->authorize('update', $user);

        return view('auth/passwords/edit', compact('user'));
    }

    public function update(User $user, UpdateUserPassword $request)
    {
        $attributes = $request->validated();

        if (Hash::check($attributes['old-password'], $user->getAuthPassword())) {
            $user->password = Hash::make($attributes['password']);
            $user->save();
        }

        return redirect("game");
    }
}
