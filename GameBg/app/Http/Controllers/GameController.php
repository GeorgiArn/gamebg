<?php

namespace App\Http\Controllers;

use App\Category;
use App\Game;
use App\GameImage;
use App\Http\Requests\StoreGame;
use App\Http\Requests\UpdateGame;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class GameController extends Controller
{
    public function index()
    {
        $games = Game::with('category')->get();
        $user = auth()->user();

        return view('game/index',
            compact('games', 'user')
        );
    }

    public function show(Game $game)
    {
        return view('game/show', compact('game'));
    }
}
