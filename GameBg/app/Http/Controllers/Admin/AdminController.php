<?php

namespace App\Http\Controllers;

use App\Category;
use App\Game;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function show()
    {
        $categories = Category::all();
        $users = User::all();
        $games = Game::all();

        return view('admin/show',
            compact('categories', 'users', 'games'));
    }
}
