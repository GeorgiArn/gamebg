<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Game;
use App\GameImage;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreGame;
use App\Http\Requests\UpdateGame;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class GameController extends Controller
{

    public function middleware($middleware, array $options = [])
    {
        return 'admin';
    }

    public function destroy(Game $game)
    {
        $this->authorize('destroy-game');

        $game->delete();

        return back();
    }

    public function create()
    {
        $this->authorize('store-game');

        $categories = Category::all();

        return view('admin/game/create', compact('categories'));
    }

    public function store(StoreGame $request)
    {
        $attributes = $request->validated();

        $game = Game::create([
            'name' => $attributes['name'],
            'description' => $attributes['description'],
            'category_id' => $attributes['category'],
            'slug' => Str::slug($attributes['name'], '-')
        ]);

        foreach ($attributes['images'] as $image) {
            GameImage::create([
                'image' => Storage::disk('public')->put('games', $image),
                'game_id' => $game->id,
                'is_main_image' => GameImage::where('game_id', $game->id)->get()->count() < 1 ? true : false
            ]);
        }

        return redirect(route('admin.show'));
    }

    public function edit(Game $game)
    {
        $this->authorize('update-game');

        $categories = Category::all();

        return view('admin/game/edit', compact('game', 'categories'));
    }

    public function update(Game $game, UpdateGame $request)
    {
        $attributes = $request->validated();

        $attributes['slug'] = Str::slug($attributes['name'], '-');
        $game->update($attributes);

        if ($request->file('images')) {
            foreach ($attributes['images'] as $imageId => $imageURL) {
                $gameImage = GameImage::find($imageId);
                $gameImage->image = Storage::disk('public')->put('games', $imageURL);

                $gameImage->update();
            }
        }

        return redirect(route('admin.show'));
    }
}
