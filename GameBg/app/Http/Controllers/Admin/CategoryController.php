<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StoreCategory;
use App\Http\Requests\UpdateCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function store(StoreCategory $request)
    {
        $attributes = $request->validated();

        Category::create([
            'name' => $attributes['name'],
            'image' => Storage::disk('public')->put('categories', $attributes['image']),
            'slug' => Str::slug($attributes['name'], '-')
        ]);

        return back();
    }

    public function update(Category $category, UpdateCategory $request)
    {
        $attributes = $request->validated();

        $attributes['slug'] = Str::slug($attributes['name'], '-');

        if ($request->file('image')) {
            $attributes['image'] = Storage::disk('public')->put('categories', $request->file('image'));
        }

        $category->update($attributes);

        return back();
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return back();
    }
}
