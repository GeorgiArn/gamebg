<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function middleware($middleware, array $options = [])
    {
        return "admin";
    }

    public function destroy(User $user)
    {
        $this->authorize('destroy-user');

        $user->delete();

        if ($user->id === auth()->user()->id) {
            return redirect('games');
        }

        return back();
    }
}
