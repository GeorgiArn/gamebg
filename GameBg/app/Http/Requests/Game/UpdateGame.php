<?php

namespace App\Http\Requests;

use App\Game;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateGame extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('update-game', auth()->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:255', Rule::unique('games')->ignore($this->route('game'))],
            'description' => ['required'],
            'category_id' => ['required', 'exists:categories,id'],
            'images.*' => ['image']
        ];
    }

    public function messages()
    {
        return [
            'images.*.image' => 'Invalid image type!'
        ];
    }
}
