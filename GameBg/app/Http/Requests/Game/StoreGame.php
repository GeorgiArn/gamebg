<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreGame extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('store-game', auth()->user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:255', Rule::unique('games')],
            'description' => ['required'],
            'category' => ['required', 'exists:categories,id'],
            'images' => ['min:6'],
            'images.*' => ['image']
        ];
    }

    public function messages()
    {
        return [
            'images.*.image' => 'Invalid image type!',
            'images.min' => 'You have to upload six images!'
        ];
    }
}
