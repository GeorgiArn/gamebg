<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        "name", "image", "slug"
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    // Relations
    public function games()
    {
        return $this->hasMany(Game::class, 'category_id');
    }

    // Helper functions
    public function getImage()
    {
        return asset($this->image);
    }
}
