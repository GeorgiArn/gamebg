<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameImage extends Model
{
    protected $fillable = [
        "image", "is_main_image", "game_id"
    ];

    // Relations
    public function game()
    {
        return $this->belongsTo(Game::class,'game_id');
    }

    // Helper functions
    public function getImage()
    {
        return asset($this->image);
    }
}
