<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function games()
    {
        return $this->belongsToMany(Game::class,
            'players_games',
            'player_id',
            'game_id');
    }

    public function addGame(Game $game)
    {
        return $this->games()->save($game);
    }

    public function removeGame(Game $game)
    {
        return $this->games()->detach($game);
    }

    public function acceptedFriends()
    {
        return $this->belongsToMany(User::class,
            'friends',
            'added_friend_id',
            'user_id');

    }

    public function addedFriends()
    {
        return $this->belongsToMany(User::class,
            'friends',
            'user_id',
            'added_friend_id');
    }

    public function friends()
    {
        $acceptedFriends = $this->acceptedFriends()->latest()->get();
        $addedFriends = $this->addedFriends()->latest()->get();

        return $acceptedFriends->merge($addedFriends);
    }

    public function addFriend(User $user)
    {
        return $this->addedFriends()->save($user);
    }

    public function removeFriend(User $user)
    {
        return $this->addedFriends()->detach($user);
    }

    public function isFriend(User $user)
    {
        return (bool) $this->addedFriends()->where('added_friend_id', $user->id)->count() || $this->acceptedFriends()->where('user_id', $user->id)->count();
    }

    public function sendMessages()
    {
        return $this->hasMany(Message::class,
            'sender_id');
    }

    public function receivedMessages()
    {
        return $this->hasMany(Message::class,
            'receiver_id');
    }

    public function messagesTimelineByUser(User $user)
    {
        $sendMessagesToUser = $this->sendMessages()->where('receiver_id', $user->id)->latest()->get();
        $receivedMessagesByUser = $this->receivedMessages()->where('sender_id', $user->id)->latest()->get();

        $messages = $sendMessagesToUser->merge($receivedMessagesByUser);
        return $messages->sortBy('created_at');
    }

    public function isReceiver(Message $message)
    {
        return $message->receiver->id === $this->id;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class,
            'users_roles',
            'user_id',
            'role_id')->withTimestamps();
    }

    public function assignRole(Role $role)
    {
        return $this->roles()->save($role);
    }

    public function isAdmin()
    {
        return (bool) $this->roles()->where('role_id', 2)->count();
    }

    public function getRouteKeyName()
    {
        return 'username';
    }

    public function getImage()
    {
        return asset($this->image);
    }
}
