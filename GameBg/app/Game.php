<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = [
        "name", "description", "slug", "category_id"
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    // Relations
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function players()
    {
        return $this->belongsToMany(User::class,
            'players_games',
            'game_id',
            'player_id');
    }

    public function images()
    {
        return $this->hasMany(GameImage::class, 'game_id');
    }

    // Helper functions
    public function isPlayedBy(User $user)
    {
        return (bool)$this->players()->where('player_id', $user->id)->count();
    }

    public function mainImage()
    {
        return $this->images()->where('is_main_image', true)->first();
    }
}
