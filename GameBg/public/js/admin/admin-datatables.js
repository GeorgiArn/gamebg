$(document).ready(function() {
    $('#users-table, #games-table').DataTable( {
        "scrollY":        "450px",
        "scrollCollapse": true,
        "paging":         false
    } );
} );
